local solution = {}

function solution.switcheroo(vals)
	local sol = ""
	for c in vals:gmatch(".") do
		if c == "a" then sol = sol .. "b" end
		if c == "b" then sol = sol .. "a" end
		if c == "c" then sol = sol .. c end
	end

	return sol
end

return solution
