local solution = {}

function solution.persistence(n)
	if n < 10 then return 0 end
	local sol = 0
	n = tostring(n)
	while true do
		sol = sol + 1
		local tmp = 1
		for c in n:gmatch(".") do
			tmp = tmp * tonumber(c)
		end
		if tmp < 10 then break end
		n = tostring(tmp)
		sol = sol + 1
	end
	return sol
end


return solution
