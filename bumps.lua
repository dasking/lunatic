local solution = {}

function solution.bumps(n)
	local nbump = 0
	for _ in n:gmatch("n") do nbump = nbump + 1 end
	if nbump <= 15 then return "Woohoo!" else return "Car Dead" end
end

return solution


