function sum_two_smallest_numbers(arr)
	table.sort(arr)
	return arr[1] + arr[2]
end

return sum_two_smallest_numbers
