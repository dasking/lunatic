local t = {}
function t.twoOldestAges(vals)
	table.sort(vals)
	return {vals[#vals-1], vals[#vals]}
end
return t
